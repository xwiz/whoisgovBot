// Fetch Users
$.ajax({
	method: 'get',
	url: "http://localhost:8000/api?resource=users",
	success: function(response){
	// Ask for confirmation before raising dispute
		var resp = JSON.parse(response).data;
		var cust_list = $('#customer_list');

		console.log(cust_list);

		for (var i = resp.length - 1; i >= 0; i--) {
			var element = '<a style="text-decoration:none;" onclick="fetch_user('+resp[i].id+')"><div class="panel"><span><img class="customer-img img-circle" src=' +resp[i].picture_url + '> ' + '<span class="customer-name hidden-xs">' +resp[i].name +'</span></span></div></a>'
			cust_list.append(element);
		}
	},
	error: function(response) {
		console.log(response);
	}
});

// Fetch User conversation
function fetch_user(userId) {
	$.ajax({
		method: 'get',
		url: "http://localhost:8000/api?resource=user&id="+userId,
		success: function(response){
		// Ask for confirmation before raising dispute
			var resp = JSON.parse(response);
			var user = resp.data;
			var conversation = resp.data.conversations;
			console.log(conversation);
			var conv_list = $('#conversaton_list');
			var user_details = $('#user_details');
			var convs_element = $('#conversations');
			var user_element = '<p><div class=""><div><img class="customer-img img-circle" src=' +user.picture_url + '></div>' + '<span class="customer-name2">' +user.name +'</span></div></p>';
			user_details.html(user_element);
			user_details.fadeIn();
			convs_element.html('');
			convs_element.hide();
			convs_element.css('opacity', 0).slideDown('slow').animate(
			    { opacity: 1 },
			    { queue: false, duration: 'slower' }
			  );
			for (var i = 0; i < conversation.length; i++) {
				var element = '<tr><td>' + conversation[i].question + '</td><td>' + conversation[i].response + '</td></tr>';
				convs_element.append(element);
			}
		},
		error: function(response) {
			console.log(response);
		}
	});
}