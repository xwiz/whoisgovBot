<?php
function getBaseUrl()
{
	return 'localhost:8000/api/';
}

function getAccessToken()
{
	return 'EAAEB4E8k9pkBAICZAT0ZCCTwmoNWZAHmnxjObadVeJLTq1VxKD0MLNiG0ZB9Dtqd1OsRGqdpV0gu0f4i8YiHZCEZAg9cS7mqpxXL6amfH1Mbk5AIXR2gu8NOsG050Ism4LNbBuPMZBtqwMBwRvHATIXwr8ur01pZAZALG556UKb4BRNypZBguoMXQT';
}

function getHubVerifyToken()
{
	return 'who_is_governor';
}

function getHeader()
{
	return $header = array(
		'content-type' => 'application/json'
	);
}

function getDatabaseConfig()
{
	return [
	    'driver' => DBDRIVER,
	    'host' => DBHOST,
	    'database' => DBNAME,
	    'username' => DBUSER,
	    'password' => DBPASS,
	    'charset' => 'utf8',
	    'collation' => 'utf8_unicode_ci',
	    'prefix' => '',
	    ];
}

function getCoatOfArms()
{
	return 'https://scontent.flos6-1.fna.fbcdn.net/v/t1.0-9/26112399_1756278177745150_5455902675650113235_n.jpg?_nc_eui2=v1%3AAeE1E4xEV0Bzh_rDMbXPWaQDM9QRO2GRM9LF0x6TwPgrOcr4e9Fq6jUb7L_0c9N4REjin-tQ90j4b6ZGYsyUD2jETv9qC-HGTJv7B4Am6EJa5A&oh=854b9b02e460a71466f47b7d47c100ca&oe=5ABD4AA0';
}

function isMale()
{
	return 'M';
}

function isFemale()
{
	return 'F';
}