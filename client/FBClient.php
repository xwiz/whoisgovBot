<?php
namespace client;
require 'vendor/autoload.php';
use \GuzzleHttp\Client;
use \GuzzleHttp\Exception\RequestException;
use \GuzzleHttp\Psr7\Request;
class FBClient
{
	protected $hubVerifyToken = 'who_is_governor';
	protected $accessToken = 'EAAEB4E8k9pkBAICZAT0ZCCTwmoNWZAHmnxjObadVeJLTq1VxKD0MLNiG0ZB9Dtqd1OsRGqdpV0gu0f4i8YiHZCEZAg9cS7mqpxXL6amfH1Mbk5AIXR2gu8NOsG050Ism4LNbBuPMZBtqwMBwRvHATIXwr8ur01pZAZALG556UKb4BRNypZBguoMXQT';
	// protected $tokken = false;
	// protected $client = null;
	function __construct()
	{
	}
	public function setHubVerifyToken($value)
	{
		return $this->hubVerifyToken = $value;
	}
	public function setAccessToken($value)
	{
		return $this->accessToken = $value;
	}
	public function verifyTokken($hub_verify_token, $challange)
	{
		try {
			if ($hub_verify_token === getHubVerifyToken()) {
				return true;
			}
			else {
				return false;
				// throw new Exception("Token not verified");
			}
		}
		catch(Exception $ex) {
			return $ex->getMessage();
		}
	}
	public function readMessage($input)
	{
		try {
			$payloads = null;
			$senderId = $input['entry'][0]['messaging'][0]['sender']['id'];
			$messageText = $input['entry'][0]['messaging'][0]['message']['text'];
			$postback = $input['entry'][0]['messaging'][0]['postback'];
			$quick_reply = $input['entry'][0]['messaging'][0]['message']['quick_reply']['payload'];
			$loctitle = $input['entry'][0]['messaging'][0]['message']['attachments'][0]['title'];
			if (!empty($postback)) {
				$payloads = $input['entry'][0]['messaging'][0]['postback']['payload'];
				if(json_decode($payloads) && json_last_error() == JSON_ERROR_NONE)
				{
					$payloads = json_decode($payloads);
				}
				return ['senderid' => $senderId, 'message' => $payloads];
			}
			if(!empty($quick_reply)) {
				$payloads = $input['entry'][0]['messaging'][0]['message']['quick_reply']['payload'];
				if(json_decode($payloads) && json_last_error() == JSON_ERROR_NONE)
				{
					$payloads = json_decode($payloads);
				}
				return ['senderid' => $senderId, 'message' => $payloads];
			}
			if (!empty($loctitle)) {
				$payloads = $input['entry'][0]['messaging'][0]['postback']['payload'];
				return ['senderid' => $senderId, 'message' => $messageText, 'location' => $loctitle];
			}
			if($messageText != '')
			{
				return ['senderid' => $senderId, 'message' => $messageText];
			}
			
			return ['senderid' => $senderId, 'message' => $postback];
			//   $payload_txt = $input['entry'][0]['messaging'][0]['message']['quick_reply']‌​['payload'];
		}
		catch(Exception $ex) {
			return $ex->getMessage();
		}
	}
}
?>