<?php
require 'config.php';
require 'vendor/autoload.php';

use \client\FBClient;
use \brain\processor\MessageProcessor;
use \brain\processor\ResponseProcessor;

use \brain\model\Database;
use \brain\seeder\DatabaseSeeder;
 
//Boot Database Connection
new Database();

// DEVELOPMENT TOOL
// Create Database Tables
try{
	$db = new Database();
	$db->createDatabase();

	// Seed Databases
	$seed = new DatabaseSeeder();
	$seed->seedStates();
	$seed->seedGovernors();
}
catch(Exception $e)
{
	error_log($e->getMessage());
}
// DEVELOPMENT TOOL ENDS HERE

$bot = new FBClient();

if (isset($_REQUEST['hub_verify_token'])) {
	if($_REQUEST['hub_verify_token'] === getHubVerifyToken())
	{	
	  echo $_REQUEST['hub_challenge'];
	  exit;
	}
}

$input = json_decode(file_get_contents('php://input'), true);

if(isset($input))
{
	// break payload to get sender_id and the request_message
	$message = $bot->readMessage($input);
	// Try to make sense of the request_message
	$msg_proc = new MessageProcessor();
	$msg_proc->comprehendMessage($message);
}