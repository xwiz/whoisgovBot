-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 22, 2017 at 04:41 PM
-- Server version: 5.7.20
-- PHP Version: 7.0.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `whogovs`
--

-- --------------------------------------------------------

--
-- Table structure for table `bot_questions`
--

CREATE TABLE `bot_questions` (
  `id` int(10) UNSIGNED NOT NULL,
  `question` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bot_responses`
--

CREATE TABLE `bot_responses` (
  `id` int(10) UNSIGNED NOT NULL,
  `response` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `conversations`
--

CREATE TABLE `conversations` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `question` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `response` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `governors`
--

CREATE TABLE `governors` (
  `id` int(10) UNSIGNED NOT NULL,
  `state_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `governors`
--

INSERT INTO `governors` (`id`, `state_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'Okezie Ikpeazu', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(2, 2, 'Bindo Jibrilla', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(3, 3, 'Udom Gabriel Emmanuel', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(4, 4, 'Willie Obiano', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(5, 5, 'Mohammed Abdullahi Abubakar', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(6, 6, 'Henry Dickson', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(7, 7, 'Samuel Ortom', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(8, 8, 'Kashim Shettima', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(9, 9, 'Benedict Ayade', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(10, 10, 'Ifeanyi Okowa', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(11, 11, 'Dave Umahi', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(12, 12, 'Godwin Obaseki', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(13, 13, 'Ayo Fayose', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(14, 14, 'Ifeanyi Ugwuanyi', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(15, 15, 'Ibrahim Hassan Dankwambo', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(16, 16, 'Owelle Rochas Okorocha', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(17, 17, 'Badaru Abubakar', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(18, 18, 'Nasir Ahmad el-Rufai', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(19, 19, 'Abdullahi Umar Ganduje', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(20, 20, 'Aminu Bello Masari', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(21, 21, 'Abubakar Atiku Bagudu', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(22, 22, 'Yahaya Bello', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(23, 23, 'Abdulfatah Ahmed', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(24, 24, 'Akinwunmi Ambode', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(25, 25, 'Umaru Tanko Al-Makura', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(26, 26, 'Abubakar Sani Bello', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(27, 27, 'Ibikunle Oyelaja Amosun', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(28, 28, 'Oluwarotimi Odunayo Akeredolu', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(29, 29, 'Rauf Aregbesola', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(30, 30, 'Isiaka Abiola Ajimobi', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(31, 31, 'Simon Lalong', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(32, 32, 'Ezenwo Nyesom Wike', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(33, 33, 'Aminu Waziri Tambuwal', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(34, 34, 'Arch. Darius Ishaku', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(35, 35, 'Ibrahim Gestate_idam', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(36, 36, 'Abdul-Aziz Yari Abubakar', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(37, 37, 'Mohammed Musa Bello', '2017-12-22 15:40:26', '2017-12-22 15:40:26');

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE `ratings` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `governor_id` int(10) UNSIGNED NOT NULL,
  `ratings` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Abia', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(2, 'Adamawa', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(3, 'Akwa Ibom', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(4, 'Anambra', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(5, 'Bauchi', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(6, 'Bayelsa', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(7, 'Benue', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(8, 'Borno', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(9, 'Cross River', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(10, 'Delta', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(11, 'Ebonyi', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(12, 'Edo', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(13, 'Ekiti', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(14, 'Enugu', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(15, 'Gombe', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(16, 'Imo', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(17, 'Jigawa', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(18, 'Kaduna', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(19, 'Kano', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(20, 'Katsina', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(21, 'Kebbi', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(22, 'Kogi', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(23, 'Kwara', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(24, 'Lagos', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(25, 'Nassarawa', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(26, 'Niger', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(27, 'Ogun', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(28, 'Ondo', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(29, 'Osun', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(30, 'Oyo', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(31, 'Plateau', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(32, 'Rivers', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(33, 'Sokoto', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(34, 'Taraba', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(35, 'Yobe', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(36, 'Zamfara', '2017-12-22 15:40:26', '2017-12-22 15:40:26'),
(37, 'Federal Capital Territory', '2017-12-22 15:40:26', '2017-12-22 15:40:26');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `fb_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` char(6) COLLATE utf8_unicode_ci NOT NULL,
  `picture_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bot_questions`
--
ALTER TABLE `bot_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bot_responses`
--
ALTER TABLE `bot_responses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conversations`
--
ALTER TABLE `conversations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `conversations_user_id_foreign` (`user_id`);

--
-- Indexes for table `governors`
--
ALTER TABLE `governors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `governors_state_id_foreign` (`state_id`);

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ratings_user_id_foreign` (`user_id`),
  ADD KEY `ratings_governor_id_foreign` (`governor_id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `states_name_unique` (`name`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_fb_id_unique` (`fb_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bot_questions`
--
ALTER TABLE `bot_questions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bot_responses`
--
ALTER TABLE `bot_responses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `conversations`
--
ALTER TABLE `conversations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `governors`
--
ALTER TABLE `governors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `ratings`
--
ALTER TABLE `ratings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `conversations`
--
ALTER TABLE `conversations`
  ADD CONSTRAINT `conversations_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `governors`
--
ALTER TABLE `governors`
  ADD CONSTRAINT `governors_state_id_foreign` FOREIGN KEY (`state_id`) REFERENCES `states` (`id`);

--
-- Constraints for table `ratings`
--
ALTER TABLE `ratings`
  ADD CONSTRAINT `ratings_governor_id_foreign` FOREIGN KEY (`governor_id`) REFERENCES `governors` (`id`),
  ADD CONSTRAINT `ratings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
