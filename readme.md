# Who Governs Nigeria

- [About](#about)
- [Project Configuration](#project-configuration)
- [API](#api)
- [Views](#views)

<a name="about"></a>
## About

This app show customer the name of the governor to which state they inquire, it can respond to basic greeting with a monotonous `Hello, I am a Robot`, it tell time and date if care to know.

<a name="project-configuration"></a>
## Project Configuration

#### Downloading the project dependencies
This project relies on some packages:
- guzzlehttp/guzzle,
- illuminate/database,
- symfony/var-dumper,
- illuminate/pagination

You would need to install `Composer` on your local machine or server in addition to `php5` or `php7` .

#### Setting up database
This project uses `mysql` database.

Create a database with name = `whogovs`, or you could edit `config.php` to set the database name to your choise name.

Another way to set you database up is to use the included `db.sql` file (found in the root folder of this project).

#### Seeding
Whichever method you decide to use in configuring you database, the required seeded table is seeded automatically. No need to run extra seeding commands.

<a name="api"></a>
## API

You can access the following resources on the app from the following endpoints

#### All Users
		`BASE_URL/api?resource=users`


#### Single User
		`BASE_URL/api?resource=user&id=USER_ID`

#### All Governors
		`BASE_URL/api?resource=governors`

#### Single Governor
		`BASE_URL/api?resource=governor&id=GOVERNOR_ID`

#### General Statistics
		`BASE_URL/api?resource=statistics`

<a name="views"></a>

## Views

There is an admin view that allows the admin see the Customer interation with the Messenger Bot

`BASE_URL/whogovsadmin.html`


## Notes

You would need an active internet connection to load assets used on the Admin View.
- Bootstrap is loaded from CDN
- JQuery is loaded from CDN

You will need to set up a facebook page and all that is required to run a messenger bot (ngrok and the likes, if you are running this app on a local machine)

#### Tutorial

https://chatbotslife.com/how-to-build-facebook-messenger-chatbot-in-php-with-structured-messages-59fb4e839f3c
