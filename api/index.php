<?php
namespace api;

require '../config.php';
require '../vendor/autoload.php';

use api\v1\RouteController;
use brain\model\Database;

//Boot Database Connection
new Database();


$resource = isset($_REQUEST['resource']) ? $_REQUEST['resource'] : '';
$id = isset($_REQUEST['id']) ? $_REQUEST['id'] : '';
$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
$sender_id = isset($_REQUEST['sender_id']) ? $_REQUEST['sender_id'] : '';

// Route to resouce
echo RouteController::getResource($resource, $id, $action, $sender_id);