<?php
namespace api\v1;
use api\v1\ResourceController;
/**
* Fetch data from the database
*/
class RouteController
{
	/**
	 * Get the resource that the request is trying to access
	 * @param type $resource 
	 * @param type|null $id 
	 * @param type|null $action 
	 * @param type|null $sender_id 
	 * @return resource
	 */
	public static function getResource($resource, $id = null, $action = null, $sender_id = null)
	{
		if(isset($_REQUEST['resource']))
		{
			if($_REQUEST['resource'] === 'user')
			{
				// Show user info with conversation and probably ratings
				$user_id = $_REQUEST['id'];
				return ResourceController::getUser($user_id);
			}
			if($_REQUEST['resource'] === 'users')
			{
				// Show all users with conversations
				return ResourceController::getUsers();
			}
			if($_REQUEST['resource'] === 'governor')
			{
				// Show governor's info with ratings statistics
				$governor_id = $_REQUEST['id'];
				return ResourceController::getGovernor($governor_id);
			}
			if($_REQUEST['resource'] === 'governors')
			{
				// Show all governors with rating statistics
				return ResourceController::getGovernors();
			}
			if($_REQUEST['resource'] === 'statistics')
			{
				// Show all governors with rating statistics
				return ResourceController::getStatics();
			}
		}
	}
}