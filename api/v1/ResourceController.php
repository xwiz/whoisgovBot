<?php
namespace api\v1;
use brain\model\UserModel;
use brain\model\GovernorModel;
use brain\model\ConversationModel;
use brain\model\RatingModel;
/**
* Fetch data from the database
*/
class ResourceController
{
	
	/**
	 * Get user information
	 * @param type|null $user_id 
	 * @return json
	 */
	public static function getUser($user_id = null)
	{
		if ($user_id == null) {
			return json_encode(['data' => "User does not exist"]);
		}
		$user = UserModel::find($user_id)->load('conversations');
		if($user)
		{
			return json_encode(['data'=>$user]);
		}
		else
		{
			return json_encode(['data' => "User does not exist"]);
		}
	}
	/**
	 * Get all users
	 * @return json
	 */
	public static function getUsers()
	{
		return json_encode(['data' => UserModel::all()]);
	}
	/**
	 * Get a governor;
	 * @param type|null $governor_id 
	 * @return json
	 */
	public static function getGovernor($governor_id = null)
	{
		if ($governor_id == null) {
			return json_encode(['data' => "Governor does not exist"]);
		}
		$governor = GovernorModel::find($governor_id);
		$governor_rating = $governor->ratings->sum('ratings');
		$gender_rates = $governor->governorGenderStatistics();
		$rates = $governor->governorRatingStatistics();
		if ($governor) {
			return json_encode(['data' =>['details'=>$governor, 'rating'=>$governor_rating, 'statistics' => ['rates' => $rates, 'gender' => $gender_rates]]]);
		}
		else
		{
			return json_encode(['data' => "Governor does not exist"]);
		}
	}
	/**
	 * Get all governors
	 * @return json
	 */
	public static function getGovernors()
	{
		return json_encode(['data' => GovernorModel::all()]);
	}
	/**
	 * Get general statistics for gender votes
	 * @return json
	 */
	public static function getStatics()
	{
		$rating = new RatingModel;
		return json_encode(['data' => $rating->allGenderStatistics()]);
	}
}