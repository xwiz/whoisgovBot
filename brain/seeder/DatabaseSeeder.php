<?php
namespace brain\seeder;
use brain\model\StateModel;
use brain\model\GovernorModel;
/**
*
*/
class DatabaseSeeder
{
	public $states = [
		["name" => "Abia"],
		["name" => "Adamawa"],
		["name" => "Akwa Ibom"],
		["name" => "Anambra"],
		["name" => "Bauchi"],
		["name" => "Bayelsa"],
		["name" => "Benue"],
		["name" => "Borno"],
		["name" => "Cross River"],
		["name" => "Delta"],
		["name" => "Ebonyi"],
		["name" => "Edo"],
		["name" => "Ekiti"],
		["name" => "Enugu"],
		["name" => "Gombe"],
		["name" => "Imo"],
		["name" => "Jigawa"],
		["name" => "Kaduna"],
		["name" => "Kano"],
		["name" => "Katsina"],
		["name" => "Kebbi"],
		["name" => "Kogi"],
		["name" => "Kwara"],
		["name" => "Lagos"],
		["name" => "Nassarawa"],
		["name" => "Niger"],
		["name" => "Ogun"],
		["name" => "Ondo"],
		["name" => "Osun"],
		["name" => "Oyo"],
		["name" => "Plateau"],
		["name" => "Rivers"],
		["name" => "Sokoto"],
		["name" => "Taraba"],
		["name" => "Yobe"],
		["name" => "Zamfara"],
		["name" => "Federal Capital Territory"],
	];
	public $governors = [
		["state_id" => "1", "name" => "Okezie Ikpeazu"],
		["state_id" => "2", "name" => "Bindo Jibrilla"],
		["state_id" => "3", "name" => "Udom Gabriel Emmanuel"],
		["state_id" => "4", "name" => "Willie Obiano"],
		["state_id" => "5", "name" => "Mohammed Abdullahi Abubakar"],
		["state_id" => "6", "name" => "Henry Dickson"],
		["state_id" => "7", "name" => "Samuel Ortom"],
		["state_id" => "8", "name" => "Kashim Shettima"],
		["state_id" => "9", "name" => "Benedict Ayade"],
		["state_id" => "10", "name" => "Ifeanyi Okowa"],
		["state_id" => "11", "name" => "Dave Umahi"],
		["state_id" => "12", "name" => "Godwin Obaseki"],
		["state_id" => "13", "name" => "Ayo Fayose"],
		["state_id" => "14", "name" => "Ifeanyi Ugwuanyi"],
		["state_id" => "15", "name" => "Ibrahim Hassan Dankwambo"],
		["state_id" => "16", "name" => "Owelle Rochas Okorocha"],
		["state_id" => "17", "name" => "Badaru Abubakar"],
		["state_id" => "18", "name" => "Nasir Ahmad el-Rufai"],
		["state_id" => "19", "name" => "Abdullahi Umar Ganduje"],
		["state_id" => "20", "name" => "Aminu Bello Masari"],
		["state_id" => "21", "name" => "Abubakar Atiku Bagudu"],
		["state_id" => "22", "name" => "Yahaya Bello"],
		["state_id" => "23", "name" => "Abdulfatah Ahmed"],
		["state_id" => "24", "name" => "Akinwunmi Ambode"],
		["state_id" => "25", "name" => "Umaru Tanko Al-Makura"],
		["state_id" => "26", "name" => "Abubakar Sani Bello"],
		["state_id" => "27", "name" => "Ibikunle Oyelaja Amosun"],
		["state_id" => "28", "name" => "Oluwarotimi Odunayo Akeredolu"],
		["state_id" => "29", "name" => "Rauf Aregbesola"],
		["state_id" => "30", "name" => "Isiaka Abiola Ajimobi"],
		["state_id" => "31", "name" => "Simon Lalong"],
		["state_id" => "32", "name" => "Ezenwo Nyesom Wike"],
		["state_id" => "33", "name" => "Aminu Waziri Tambuwal"],
		["state_id" => "34", "name" => "Arch. Darius Ishaku"],
		["state_id" => "35", "name" => "Ibrahim Gestate_idam"],
		["state_id" => "36", "name" => "Abdul-Aziz Yari Abubakar"],
		["state_id" => "37", "name" => "Mohammed Musa Bello"],
	];
	/**
	* Seed States table
	*/
	public function seedStates()
	{
		$states = $this->states;
		foreach ($states as $value) {
			$state = new StateModel();
			$state->name = $value['name'];
			$state->save();
		}
		return;
	}
	/**
	* Seed Governor table
	*/
	public function seedGovernors()
	{
		$governors = $this->governors;
		foreach ($governors as $key => $value) {
			$governor = new GovernorModel();
			$governor->state_id = $value['state_id'];
			$governor->name = $value['name'];
			$governor->save();
		}
		return;
	}
}