<?php
namespace brain\processor;
use brain\model\ConversationModel;
/**
* Process the content of the message that was sent by the user
*/
class MessageProcessor
{
	// States in Nigeria, this has been separated to allow a user type federal capital or Ibom and still get a result for it
	public $states = ['abia', 'adamawa', 'akwa-ibom', 'akwa ibom','anambra', 'bauchi', 'bayelsa', 'benue', 'borno', 'cross river', 'cross-river', 'delta', 'ebonyi', 'edo', 'ekiti', 'enugu', 'federal capital territory', 'fct', 'gombe', 'imo', 'jigawa', 'kaduna', 'kano', 'katsina', 'kebbi', 'kogi', 'kwara', 'lagos', 'nassarawa', 'niger', 'ogun', 'ondo', 'osun', 'oyo', 'plateau', 'rivers', 'sokoto', 'taraba', 'yobe', 'zamfara'
	];
	/**
	* Try to guess if the user is greeting
	* @param type $message
	* @return type
	*/
	public function isGreeting($message)
	{
		if(in_array('hello', $message) || in_array('hi', $message) || (in_array('good', $message) && in_array('morning', $message) || in_array('afternoon', $message) || in_array('evening', $message)))
		{
			return [true, $message];
		}
		return [false];
	}
	/**
	* Try to guess if the user asks for the time
	* @param type $message
	* @return type
	*/
	public function isTime($message)
	{
		if($message == 'what time is it?' || $message == 'what time is it' || $message == 'what says the time?' || $message == 'what says the time' || (in_array('time', $message) && in_array('what', $message)))
		{
			return [true, $message];
		}
		return [false];
	}
	/**
	* Try to guess if the user asks for the Date
	* @param type $message
	* @return type
	*/
	public function isDate($message)
	{
		if($message == 'what day is it?' || $message == 'what day is it' || $message == 'what is today\'s date?' || $message == 'what is today' || (in_array('day', $message) && in_array('what', $message)) || (in_array('date', $message) && in_array('what', $message)))
		{
			return [true, $message];
		}
		return [false];
	}
	/**
	* Try to guess if the user message has 'governor' and 'state_name'
	* @param type $message
	* @return type
	*/
	public function hasGovernorAndState($message)
	{
		foreach ($message as $msg) {
			if(in_array('governor', $message) && in_array($msg, $this->states))
			{
				return [true, $message, $msg];
			}
		}
		return [false];
	}
	/**
	* Try to guess if the user message has 'governor' only
	* @param type $message
	* @return type
	*/
	public function hasGovernorOnly($message)
	{
		foreach ($message as $msg) {
			if(!in_array($msg, $this->states) && $msg == 'governor')
			{
				return [true, $message, $msg];
			}
		}
		return [false];
	}
	/**
	* Try to guess if the user message has 'governor' and 'state_name'
	* @param type $message
	* @return type
	*/
	public function hasStateOnly($message)
	{
		foreach ($message as $msg) {
			if(in_array($msg, $this->states) && !in_array('governor', $message))
			{
				return [true, $message, $msg];
			}
		}
		return [false];
	}
	/**
	* Guess if the user clicks the Rate Governor Button
	* @param type $message
	* @return type
	*/
	public function isRateGovernor($message)
	{
		if(gettype($message) == 'object')
		{
			if($message->action === 'RATE_THE_GOVERNOR')
			{
				return [true, $message->description, $message->queries->gov_id];
			}
		}
		return [false];
	}
	/**
	* Guess if the user clicks the Rate Governor Button
	* @param type $message
	* @return type
	*/
	public function isSetGovernorRating($message)
	{
		if(gettype($message) == 'object')
		{
			if($message->action === 'SET_GOVERNOR_RATING')
			{
				return [true, $message->description, ['gov_id' => $message->queries->gov_id, 'rate' => $message->queries->rate]];
			}
		}
		return [false];
	}
	/**
	* Try to guess if the user message has 'governor' and 'state_name'
	* @param type $message
	* @return type
	*/
	public function hasCreatorOnly($message)
	{
		foreach ($message as $msg) {
			if((!in_array($msg, $this->states) && !in_array('governor', $message) && in_array('creator', $message)) || (!in_array($msg, $this->states) && !in_array('governor', $message) && in_array('created', $message)))
			{
				return [true, $message];
			}
		}
		return [false];
	}
	/**
	* Try to guess if the user message has 'governor' and 'state_name'
	* @param type $message
	* @return type
	*/
	public function incomprehensible($message)
	{
		foreach ($message as $msg) {
			if((in_array($msg, $this->states) || $msg == 'governor' || $msg == 'created' || $msg == 'creator') || $msg != '')
			{
				return [FALSE];
			}
		}
		return [TRUE, $message];
	}
	public function comprehendMessage($input)
	{
		try {
			$messageText = strtolower($input['message']);
			$messagePayload = $input['message'];
			$sender_id = $input['senderid'];
			$msgarray = explode(' ', $messageText);
			$response_proc = new ResponseProcessor($sender_id);
			$is_greeting = $this->isGreeting($msgarray);
			$is_time = $this->isTime($msgarray);
			$is_date = $this->isDate($msgarray);
			$has_state = $this->hasStateOnly($msgarray);
			$has_gov_state = $this->hasGovernorAndState($msgarray);
			$has_gov = $this->hasGovernorOnly($msgarray);
			$is_rate_gov = $this->isRateGovernor($messagePayload);
			$is_set_gov_rate = $this->isSetGovernorRating($messagePayload);
			$has_creator = $this->hasCreatorOnly($msgarray);
			$no_sense = $this->incomprehensible($msgarray);
			if($is_greeting['0']) {
				// Return greeting to the user
				return $response_proc->getGreeting($is_greeting['1'], $sender_id);
			}
			elseif($is_time['0']) {
				// Tell the user what time it is
				return $response_proc->getTime($is_time['1'], $sender_id);
			}
			elseif($is_date['0']) {
				// Tell the user what time it is
				return $response_proc->getDate($is_date['1'], $sender_id);
			}
			elseif($has_state['0']) {
				// Check if the meassage has a state name in it, if it does, return basic information about the state with the governor name
				return $response_proc->getGovernorInformation($has_state['1'], $has_state['2'], $sender_id);
			}
			elseif ($has_gov_state['0']) {
				// Check if 'Governor' is contained in the message and a state name
				return $response_proc->getGovernorInformation($has_gov_state['1'], $has_gov_state['2'], $sender_id);
			}
			elseif($has_gov['0']) {
				// There is 'governor' in the message but no 'state' name specified
				return $response_proc->askStateName($has_gov['1'], $has_gov['2'], $sender_id);
			}
			elseif($is_rate_gov['0']) {
				// Fetch buttons form to rate a Governor
				return $response_proc->getRatingForm($is_rate_gov['1'], $is_rate_gov['2'], $sender_id);
			}
			elseif($is_set_gov_rate['0']) {
				// Set the Governor rating
				return $response_proc->setGovernorRating($is_set_gov_rate['1'], $is_set_gov_rate['2'], $sender_id);
			}
			elseif($has_creator['0']) {
				// Return the Creator Basic information
				return $response_proc->getCreator($has_creator['1'], $sender_id);
			}
			elseif($no_sense['0']) {
				// if the message is incomprehensible
				return $response_proc->incomprehensibleMessage($no_sense['1'], $sender_id);
			}
			return false;
		}
		catch(RequestException $e) {
			$response = json_decode($e->getResponse()->getBody(true)->getContents());
			file_put_contents("test.json", json_encode($response));
			return $response;
		}
	}
}