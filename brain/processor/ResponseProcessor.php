<?php
namespace brain\processor;
use client\FBClient;
use \GuzzleHttp\Client;
use \GuzzleHttp\Exception\RequestException;
use \GuzzleHttp\Psr7\Request;
use Carbon\Carbon;
use brain\model\StateModel;
use brain\model\GovernorModel;
use brain\model\UserModel;
use brain\model\ConversationModel;
use brain\model\RatingModel;
/**
*
*/
class ResponseProcessor
{
	public $user = null;
	function __construct($sender_id)
	{
		$data = ['fields' => 'first_name,last_name,profile_pic,gender,locale,timezone', 'access_token' => getAccessToken()];
		$user_info = $this->getResponse($sender_id, $data);
		if(UserModel::where('fb_id', '=', $sender_id)->count() < 1)
		{
			$user = new UserModel();
			$user->name = $user_info->last_name.' '.$user_info->first_name;
			$user->fb_id = $sender_id;
			$user->gender = $user_info->gender == 'male' ? isMale() : isFemale();
			$user->picture_url = $user_info->profile_pic;
			$user->locale = $user_info->locale;
			$user->timezone = $user_info->timezone;
			$user->save();
			$this->user = $user;
		}
		else
		{
			$user = UserModel::where('fb_id', '=', $sender_id)->first();
			$user->picture_url = $user_info->profile_pic;
			$user->locale = $user_info->locale;
			$user->timezone = $user_info->timezone;
			$user->save();
			$this->user = $user;
		}
	}
	
	/**
	* Compose the response to be sent back to the user using generic template
	* @param type $answer
	* @param type $sender_id
	* @return type
	*/
	public function composeGenericTemplateResponse($title = '', $subtitle = '', $item_url = '', $image_url = '', $buttons = '', $default_action = '', $sender_id)
	{
		$template = [
			"attachment" => [
			"type" => "template",
			"payload" => [
			"template_type" => "generic",
			"elements" => [[
			"title" => $title,
			"item_url" => $item_url,
			"image_url" => $image_url,
			"subtitle" => $subtitle,
			"buttons" => $buttons
			]]
		]]];
		return [ 'recipient' => ['id' => $sender_id], 'message' => $template, 'access_token' => getAccessToken()];
	}
	/**
	* Compose the response to be sent back to the user using button template
	* @param type $answer
	* @param type $sender_id
	* @return type
	*/
	// title, subtitle, image_url, buttons[type, title, url? payload?]
	public function composeButtonTemplateResponse($text = '', $buttons = '', $sender_id)
	{
		$template = [
			"attachment" => [
			"type" => "template",
			"payload" => [
			"template_type" => "button",
			"text" => $text,
			"buttons" => $buttons
		]]];
		return [ 'recipient' => ['id' => $sender_id], 'message' => $template, 'access_token' => getAccessToken()];
	}
	/**
	* Compose the response to be sent back to the user using uqick replies
	* @param type $answer
	* @param type $sender_id
	* @return type
	*/
	// title, subtitle, image_url, buttons[type, title, url? payload?]
	public function composeQuickReplies($text = '', $replies = '', $sender_id)
	{
		// error_log(var_dump(json_encode($replies)));
		$message = [
			"text" => $text,
			"quick_replies" => $replies
		];
		return [ 'recipient' => ['id' => $sender_id], 'message' => $message, 'access_token' => getAccessToken()];
	}
	/**
	* Compose the response to be sent back to the user
	* @param type $answer
	* @param type $sender_id
	* @return type
	*/
	public function composeTextResponse($answer, $sender_id)
	{
		$answer = ['text' => $answer];
		return [ 'recipient' => ['id' => $sender_id], 'message' => $answer, 'access_token' => getAccessToken()];
	}
	/**
	* Get a response
	* @param type $data
	* @return type
	*/
	public function getResponse($sender_id, $data)
	{
		$client = new Client();
		$header = getHeader();
		$url = "https://graph.facebook.com/v2.6/".$sender_id;
		$response = $client->get($url.'?fields='.$data['fields'].'&access_token='.$data['access_token']);
		return json_decode($response->getBody());
	}
	/**
	* Post a reply
	* @return type
	*/
	public function postResponse($response)
	{
		// Guzzle
		$client = new Client();
		$header = getHeader();
		$url = "https://graph.facebook.com/v2.6/me/messages";
		return $client->post($url, ['query' => $response, 'headers' => $header]);
	}
	/**
	* Save conversation history
	* @param type
	* @return type
	*/
	public function saveConveration($question, $response, $user_id)
	{
		$conversation = new ConversationModel();
		$conversation->user_id = $user_id;
		$conversation->question = gettype($question) == 'array'? implode(' ', $question) : $question;
		$conversation->response = $response;
		$conversation->save();
		return;
	}
	/**
	* Get a greeting back to the user
	* @param type $input
	* @param type $sender_id
	* @return type
	*/
	public function getGreeting($input, $sender_id)
	{
		// Get greeting
		$answer = "Hello, I am a robot";
		$response = $this->composeTextResponse($answer, $sender_id);
		$this->saveConveration($input, $answer, $this->user->id);
		return $this->postResponse($response);
	}
	/**
	* Get a current time back to the user
	* @param type $input
	* @param type $sender_id
	* @return type
	*/
	public function getTime($input, $sender_id)
	{
		// Get time
		$answer = "The time is ".Carbon::now()->addHour($this->user->timezone)->format('g:i A');
		$response = $this->composeTextResponse($answer, $sender_id);
		$this->saveConveration($input, $answer, $this->user->id);
		return $this->postResponse($response);
	}
	/**
	* Get a current date back to the user
	* @param type $input
	* @param type $sender_id
	* @return type
	*/
	public function getDate($input, $sender_id)
	{
		// Get date
		$answer = "Today is ".Carbon::now()->toFormattedDateString();
		$response = $this->composeTextResponse($answer, $sender_id);
		$this->saveConveration($input, $answer, $this->user->id);
		return $this->postResponse($response);
	}
	/**
	* Ask the user to specify the governor->name of the state they would like to know about;
	* @param type $message
	* @return type
	*/
	public function askStateName($input, $sender_id)
	{
		$answer = "Please which state governor's name would you like to know";
		error_log($input);
		$response = $this->composeTextResponse($answer, $sender_id);
		$this->saveConveration($input, $answer, $this->user->id);
		return $this->postResponse($response);
	}
	/**
	* Reply the user with the Governor information requested
	* @param type $message
	* @return type
	*/
	public function getGovernorInformation($message = null, $input, $sender_id)
	{
		// fetch information from the model to respond to the user
		$result = StateModel::where('name', '=', $input)->first();
		$subtitle = ucwords($input).' State Governor.';
		$title = 'Governor '.$result->governor->name;
		$gov_slug = implode('_',explode(' ', ucwords($result->governor->name)));
		$default_action = '';
		$item_url = 'https://en.wikipedia.org/wiki/'.$gov_slug;
		$image_url = getCoatOfArms();
		$buttons = [
				["type" => "postback", "payload" => json_encode(['action' => 'RATE_THE_GOVERNOR', 'queries' => ['gov_id' => $result->governor->id], 'description' => 'Rate Governor '.$result->governor->name]), "title" => "Rate Governor"]
		];
		$response = $this->composeGenericTemplateResponse($title, $subtitle, $item_url, $image_url, $buttons, $default_action, $sender_id);
		$this->saveConveration($message, $title, $this->user->id);
		return $this->postResponse($response);
	}
	/**
	* Reply the user with a basic detail of the state
	* @param type $message
	* @return type
	*/
	public function getStateBasicInformation($message = null, $input, $sender_id)
	{
		$result = StateModel::where('name', '=', $input)->first();
		$answer = $result->governor->name;
		$response = $this->composeTextResponse($answer, $sender_id);
		$this->saveConveration($message, $answer, $this->user->id);
		return $this->postResponse($response);
		// call an api that has this information
		// OR
		// Return the full state name and the governor with the governor rating
	}
	/**
	* Return a set of five buttons that would allow the user rate the Governor
	* @param type $input
	* @param type $sender_id
	* @return Buttons
	*/
	public function getRatingForm($message = null, $input, $sender_id)
	{
		$governor = GovernorModel::find($input)->name;
		$text = "Please choose a rating you would like to give to Governor ".$governor;
		$replies =[
			["content_type" => "text", "payload" => json_encode(['action' => 'SET_GOVERNOR_RATING', 'queries' =>['rate' => 1, 'gov_id' => $input], 'description' => "Set Governor ".$governor. " ratings to 1"]), "title" => "1 star"],
			["content_type" => "text", "payload" => json_encode(['action' => 'SET_GOVERNOR_RATING', 'queries' =>['rate' => 2, 'gov_id' => $input], 'description' => "Set Governor ".$governor. " ratings to 2"]), "title" => "2 stars"],
			["content_type" => "text", "payload" => json_encode(['action' => 'SET_GOVERNOR_RATING', 'queries' =>['rate' => 3, 'gov_id' => $input], 'description' => "Set Governor ".$governor. " ratings to 3"]), "title" => "3 stars"],
			["content_type" => "text", "payload" => json_encode(['action' => 'SET_GOVERNOR_RATING', 'queries' =>['rate' => 4, 'gov_id' => $input], 'description' => "Set Governor ".$governor. " ratings to 4"]), "title" => "4 stars"],
			["content_type" => "text", "payload" => json_encode(['action' => 'SET_GOVERNOR_RATING', 'queries' =>['rate' => 5, 'gov_id' => $input], 'description' => "Set Governor ".$governor. " ratings to 5"]), "title" => "5 stars"],
		];
		$response = $this->composeQuickReplies($text, $replies, $sender_id);
		$this->saveConveration($message, $text, $this->user->id);
		return $this->postResponse($response);
	}
	public function setGovernorRating($message = null, $input, $sender_id)
	{
		error_log(var_dump($message));
		$rate = $input['rate'];
		$gov_id = $input['gov_id'];
		$governor = GovernorModel::find($gov_id)->name;
		$user_id = $this->user->id;
		// if user already rated governor, update the rating
		if(RatingModel::where('user_id', '=', $user_id)->where('governor_id', '=', $gov_id)->count() > 0)
		{
			// Update rating
			$rating = RatingModel::where('user_id', '=', $this->user->id)->where('governor_id', '=', $gov_id)->first();
			$rating->ratings = $rate;
			$rating->save();
			$text = "Thank you, your rating for Governor ".$governor." has been updated.";
		}
		else
		{
			// Create a new rating
			$rating = new RatingModel;
			$rating->user_id = $user_id;
			$rating->governor_id = $gov_id;
			$rating->ratings = $rate;
			$rating->save();
			$text = "Thank you, your rating for Governor ".$governor." has been received.";
		}
		$response = $this->composeTextResponse($text, $sender_id);
		$this->saveConveration($message, $text, $this->user->id);
		return $this->postResponse($response);
	}
	/**
	* Get creators name
	* @return type
	*/
	public function getCreator($input, $sender_id)
	{
		$answer = "If you are really interested in who created me, here he is, Abimbola Ayodeji Oweme is his name";
		$response = $this->composeTextResponse($answer, $sender_id);
		$this->saveConveration($input, $answer, $this->user->id);
		return $this->postResponse($response);
	}
	public function incomprehensibleMessage($input, $sender_id)
	{
		$answer = "I do not seem to understand what you are asking, for I am but only a child";
		$response = $this->composeTextResponse($answer, $sender_id);
		$this->saveConveration($input, $answer, $this->user->id);
		return $this->postResponse($response);
	}
}