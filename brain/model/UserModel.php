<?php
namespace brain\model;
use \brain\model\BaseModel;
/**
* State Database Model
*/
class UserModel extends BaseModel
{
	
	protected $table = 'users';
	protected $fillable = ['fb_id', 'name'];
	/**
	* Return User conversation
	* @return Array
	*/
	public function conversations()
	{
		return $this->hasMany('brain\model\ConversationModel', 'user_id');
	}
}