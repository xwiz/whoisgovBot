<?php
namespace brain\model;
use \brain\model\BaseModel;
/**
* State Database Model
*/
class ConversationModel extends BaseModel
{
	
	protected $table = 'conversations';
	protected $fillable = ['user_id', 'question', 'response'];
	public function user()
	{
		return $this->belongsTo('brain\model\UserModel');
	}
}