<?php
namespace brain\model;
use \brain\model\BaseModel;
/**
* State Database Model
*/
class StateModel extends BaseModel
{
	
	protected $table = 'states';
	protected $fillable = ['name'];
	public function governor()
	{
		return $this->hasOne('brain\model\GovernorModel', 'state_id');
	}
}