<?php
namespace brain\model;
use Illuminate\Database\Capsule\Manager as Capsule;
/**
*
*/
class Database
{
	function __construct()
	{
		$capsule = new Capsule;
		$capsule->addConnection(getDatabaseConfig());
		$capsule->setAsGlobal();
		$capsule->bootEloquent();
	}
	/**
	 * Create tables on the database
	 */
	public function createDatabase()
	{
		try{
			Capsule::schema()->create('users', function ($table) {
			$table->increments('id');
			$table->string('name');
			$table->string('fb_id')->unique();
			$table->char('gender', 1);
			$table->string('picture_url')->nullable();
			$table->string('locale')->nullable();
			$table->string('timezone', 3);
			$table->timestamps();
			});
			Capsule::schema()->create('states', function ($table) {
			$table->increments('id');
			$table->string('name')->unique();
			$table->timestamps();
			});
			Capsule::schema()->create('governors', function ($table) {
			$table->increments('id');
			$table->integer('state_id')->unsigned();
			$table->string('name', 100);
			$table->foreign('state_id')->references('id')->on('states')->onDelete('restrict');
			$table->timestamps();
			});
			Capsule::schema()->create('conversations', function ($table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->string('question');
			$table->text('response')->nullable();
			$table->foreign('user_id')->references('id')->on('users');
			$table->timestamps();
			});
			Capsule::schema()->create('ratings', function ($table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->integer('governor_id')->unsigned();
			$table->integer('ratings')->unsigned();
			$table->foreign('user_id')->references('id')->on('users');
			$table->foreign('governor_id')->references('id')->on('governors');
			$table->timestamps();
			});
			Capsule::schema()->create('bot_questions', function ($table) {
			$table->increments('id');
			$table->string('question');
			$table->timestamps();
			});
			Capsule::schema()->create('bot_responses', function ($table) {
			$table->increments('id');
			$table->string('response');
			$table->timestamps();
			});
			return;
		}
		catch(Exception $e) {
			return error_log($e->getMessage());
		};
	}
}