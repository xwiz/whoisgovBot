<?php
namespace brain\model;
use \brain\model\BaseModel;
/**
* State Database Model
*/
class RatingModel extends BaseModel
{
	
	protected $table = 'ratings';
	protected $fillable = ['user_id', 'governor_id', 'ratings'];
	public function governor()
	{
		return $this->belongsTo('brain\model\GovernorModel');
	}
	public function user()
	{
		return $this->belongsTo('brain\model\UserModel', 'user_id');
	}
	/**
	 * Get the statistics for Male and Female ratings
	 * @return array
	 */
	public function allGenderStatistics()
	{
		$ratings = $this::all();
		$male_ratings = 0;
		$female_ratings = 0;
		foreach ($ratings as $rating) {
			$gender = $rating->user->gender;
			if($gender == 'M')
			{
				$male_ratings += $rating->ratings;
			}
			else
			{
				$female_ratings += $rating->ratings;
			}
		}
		return ['male' => $male_ratings, 'female' => $female_ratings];
	}
}