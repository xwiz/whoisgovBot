<?php
namespace brain\model;
use \brain\model\BaseModel;
/**
* State Database Model
*/
class GovernorModel extends BaseModel
{
	
	protected $table = 'governors';
	protected $fillable = ['state_id', 'name'];
	protected $hidden = ['ratings'];
	public function state()
	{
		return $this->belongsTo('brain\model\StateModel');
	}
	public function ratings()
	{
		return $this->hasMany('brain\model\RatingModel', 'governor_id');
	}
	public function votes()
	{
		// Calculate the Governor ratings
		return $this->ratings()->count();
	}
	/**
	 * Get the ratings statistics for a governor
	 * @return array
	 */
	public function governorGenderStatistics()
	{
		$ratings = $this->ratings;
		$male_ratings = 0;
		$female_ratings = 0;
		foreach ($ratings as $rate) {
			$gender = $rate->user->gender;
			if($gender == 'M')
			{
				$male_ratings = $male_ratings + $rate->ratings;
			}
			else
			{
				$female_ratings = $female_ratings + $rate->ratings;
			}
		}
		return ['male' => $male_ratings, 'female' => $female_ratings];
	}
	/**
	 * Get the ratings rates statistics for a governor (hahahahahaha)
	 * @return array
	 */
	public function governorRatingStatistics()
	{
		$ratings = $this->ratings;
		$rates_one = 0;
		$rates_two = 0;
		$rates_three = 0;
		$rates_four = 0;
		$rates_five = 0;
		foreach ($ratings as $rate) {
			if($rate->ratings == 1)
			{
				$rates_one++;
			}
			elseif($rate->ratings == 2)
			{
				$rates_two++;
			}
			elseif($rate->ratings == 3)
			{
				$rates_three++;
			}
			elseif($rate->ratings == 4)
			{
				$rates_four++;
			}
			elseif($rate->ratings == 5)
			{
				$rates_five++;
			}
		}
		return ['rates_one' => $rates_one, 'rates_two' => $rates_two, 'rates_three' => $rates_three, 'rates_four' => $rates_four, 'rates_five' => $rates_five];
	}
}