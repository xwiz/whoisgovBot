<?php
namespace brain;
/**
* Database Connector
*/
class DbConnector
{
	protected $server_name = 'localhost';
	protected $db_username = 'root';
	protected $db_password = '';
	protected $db_name = 'whoisgov';
	function __construct()
	{
		// Create connection
		$conn = new mysqli($server_name, $db_username, $db_password);
		// Check connection
		if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
		}
		if($conn->query($test_sql) === FALSE)
		{
			// Create database
			$this->createDb();
		}
		return $this->findDb();
	}
	/**
	* Create Database
	* @param string $query
	* @return type
	*/
	public function createDb()
	{
		$query = 'CREATE DATABASE '.$this->getDbName();
		$create_users =
		return $this->runQuery($query);
	}
	/**
	* Get the Database name
	* @return type
	*/
	public function getDbName()
	{
		return $this->db_name;
	}
	/**
	* Checks if the database already exists
	* @return type
	*/
	public function findDb()
	{
		$query = 'USE '.$this->getDbName();
		if($conn->query($query) === FALSE)
		{
			// Create database
			$db_name = $this->getDbName();
			return $this->createDb();
		}
	}
	/**
	* Run a query
	* @param type|null $query
	* @return type
	*/
	public function runQuery($query=NULL)
	{
		if($query == NULL)
		{
			return false;
		}
		return $conn->query($query);
	}
	/** __________________________________________________
		i guess this section of the code should be moved
		the individual models, it kind of makes the code
		cleaner that way
	__________________________________________________*/
	/**
	* Run Select query
	* @param type $table
	* @param type $column
	* @param type $value
	* @return type
	*/
	public function select($table, $column, $value)
	{
		$query = 'SELECT * FROM '.$table. 'WHERE '.$column.' = '.$value;
		$result = $this->runQuery($query);
		return $result;
	}
	public function update($value='')
	{
		// Update a model, i guess this should be done in the model itself
	}
	public function search($query_string)
	{
		// Run a search on the database using `like`
	}
	/** __________________________________________________
		Section ends here
	__________________________________________________*/
	/**
	* Destroy class instance
	*/
	public function __destruct()
	{
		$conn->close();
	}
}